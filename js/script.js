$(document).ready(function () {
  $("#myPoke").on("click", function () {
    //Appelle pour avoir un pokémon
    let pokeUrl = "https://pokeapi.co/api/v2/pokemon/?offset=0&limit=898";
    let param = document.getElementById("pokemon").value;

    $("#test1").css("display", "block");

    fetch(pokeUrl)
      .then((response) => response.json())
      .then((data) => {
        //console.log(data);
        let pokemons = data.results;
        //console.log(pokemons);

        //console.log(url);

        //En premier lieu on récupère les données sur le pokémon nom et url

        for (let i = 0; i < pokemons.length; i++) {
          if (pokemons[i].name == param) {
            let name = pokemons[i].name;
            let url = pokemons[i].url;

            //console.log(name);

            //extraire les données du pokemon

            fetch(url)
              .then((rep) => rep.json())
              .then((data) => {
                //avoir les données précise du pokémon

                const id = data.id;
                const nom = data.forms[0].name;
                let nomFinal = nom[0].toUpperCase().split("") + nom.slice(1);
                let height = data.height;
                let poids = data.weight;
                let type = data.types[0].type.name;
                let typeFinal = type[0].toUpperCase().split("") + type.slice(1);

                //On change la couleur de fond selon le type du pokemon

                if (typeFinal == "Electric") {
                  $("#test1").css("background", "rgb(240, 218, 0)");
                } else if (typeFinal == "Water") {
                  $("#test1").css("background", "rgb(65, 117, 241)");
                } else if (typeFinal == "Fire") {
                  $("#test1").css("color", "rgb(255, 255, 255)");
                  $("#test1").css("background", "rgb(220, 17, 21)");
                } else if (typeFinal == "Grass") {
                  $("#test1").css("background", "rgb(30, 200, 61)");
                } else if (typeFinal == "Psychic") {
                  $("#test1").css("background", "rgb(40, 35, 161)");
                  $("#test1").css("color", "rgb(255, 255, 255)");
                } else if (typeFinal == "Ghost") {
                  $("#test1").css("background", "rgb(102, 88, 105)");
                  $("#test1").css("color", "rgb(255, 255, 255)");
                } else {
                  $("#test1").css("background", "rgb(245, 177, 11)");
                  $("#test1").css("color", "rgb(10, 10, 10)");
                }

                //récuperations des variable pour afficher après dans le html;
                console.log(typeFinal);

                console.log(poids, height, nomFinal);

                let url3 = `https://pokeapi.co/api/v2/pokemon-species/${id}`;

                fetch(url3)
                  .then((response) => response.json())
                  .then((datas) => {
                    console.log(datas);
                    let resum = datas.flavor_text_entries[24].flavor_text;

                    //console.log(resum);

                    let frenchName = datas.names[4];

                    let nameFrenchFinal = frenchName.name;

                    //console.log(nameFrenchFinal);

                    //Afficher la carte pokemon

                    let pokeImage = `https://pokeres.bastionbot.org/images/pokemon/${id}.png`;

                    document.getElementById(
                      "img"
                    ).innerHTML = `<img id='image1' src="${pokeImage}" alt="image pokemon" />`;

                    document.getElementById(
                      "affiche"
                    ).innerHTML = `<p><strong>Id</strong> : ${id}</p><p><strong>Nom</strong> : ${nameFrenchFinal}</p><p><strong>Son poids </strong>: ${
                      poids / 10
                    } kg </p><p><strong>Hauteur</strong> : ${
                      height / 10
                    } m</p><p><strong>Type</strong> : ${typeFinal}</p><p><strong>Résumé : </strong>${resum}</p>`;
                  });
              });
          }
        }
      });
  });

  //Afficher les 200 premier pokémons

  const colors = {
    Fire: "#FDDFDF",
    Grass: "#DEFDE0",
    Electric: "#FCF7DE",
    Water: "#DEF3FD",
    Ground: "#f4e7da",
    Rock: "#d5d5d4",
    Fairy: "#fceaff",
    Poison: "#98d7a5",
    Bug: "#f8d5a3",
    Dragon: "#97b3e6",
    Psychic: "#eaeda1",
    Flying: "#F5F5F5",
    Fighting: "#E6E0D4",
    Normal: "#F5F5F5",
  };

  let poke_container = document.getElementById("container");
  let pokemon_number = 200;

  //On crée une boucle de 1 à 200

  const fetchPokemon = async () => {
    for (let i = 1; i <= pokemon_number; i++) {
      await getPokemon(i);
    }
  };

  //dans la fonction fetchPokemon, on appelle la fonction getPokemon et dans cette fonction on appelle l'api

  const getPokemon = async (id) => {
    const url = `https://pokeapi.co/api/v2/pokemon/${id}`;

    const res = await fetch(url);
    const pokemon = await res.json();
    createPokemonCard(pokemon);
  };

  fetchPokemon();

  //Pour afficher nos cartes pokemons
  //On crée un élément div.

  function createPokemonCard(pokemon) {
    const pokemonElt = document.createElement("div");

    pokemonElt.classList.add("pokemon");

    let nomFinal =
      pokemon.name[0].toUpperCase().split("") + pokemon.name.slice(1);

    let type = pokemon.types[0].type.name;
    let typeFinal = type[0].toUpperCase().split("") + type.slice(1);
    // console.log(typeFinal);

    const color = colors[typeFinal];

    pokemonElt.style.backgroundColor = color;

    const pokeInnerHTML = `
      <div class='card'>
        <div class="img-container">
            <img src="https://pokeres.bastionbot.org/images/pokemon/${pokemon.id}.png" alt="${nomFinal}" />
        </div>
        <div class='info'>
        <h3><strong>Id </strong>: ${pokemon.id}</h3>
        <h3 class='name'><strong>Nom</strong> : ${pokemon.name}</h3>
        <h4 class='type'><strong>Type</strong> : <span>${typeFinal}</span></h4>
        </div>
        </div>`;

    pokemonElt.innerHTML = pokeInnerHTML;

    poke_container.appendChild(pokemonElt);
  }

  let input = document.getElementById("pokemon");

  let pokeUrlDeux = "https://pokeapi.co/api/v2/pokemon/?offset=0&limit=898";

  fetch(pokeUrlDeux)
    .then((response) => response.json())
    .then((data) => {
      //console.log(data);
      let pokemonResult = data.results;
      //console.log(pokemonResult);

      let noms = pokemonResult.map((elt) => elt.name);

      console.log(noms);

      let tab = [];

      for (let i = 0; i < noms.length; i++) {
        tab.push(noms[i]);
      }

      console.log(tab);

      //Essayer de faire une liste

      $("#pokemon").autocomplete({
        source: tab,
      });
    });
});
